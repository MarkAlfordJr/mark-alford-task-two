//
//  SecondViewController.swift
//  MarkAlfordTaskTwo
//
//  Created by Mark Alford on 8/12/21.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {

    //MARK: - Properties
    var secTextValue: String? = ""

    
    //MARK: - UI element
    @IBOutlet weak var secVCTextField: UITextField!
    
    
    //MARK: - Action
    @IBAction func sendToFirstVCBtn(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "toFirstVC", sender: self)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        secVCTextField.text = secTextValue ?? "nothing"
        
    }
    
    //MARK: - TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        secVCTextField.endEditing(true)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if secVCTextField.text != ""{
            return true
        } else {
            secVCTextField.placeholder = "please enter something"
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        secVCTextField.endEditing(true)
    }
    
    // MARK: - Navigation
    
    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toFirstVC" {
            let destinationFirstVC = segue.destination as! ViewController
            destinationFirstVC.textValue = secVCTextField.text ?? "nothing"
        }
    }
    

}
