//
//  ViewController.swift
//  MarkAlfordTaskTwo
//
//  Created by Mark Alford on 8/12/21.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    //MARK: - Properties
    var textValue: String? = ""
    
    //MARK: - UI Element
    @IBOutlet weak var firstVCTextField: UITextField!
    
    
    //MARK: - Action
    @IBAction func sendToSecVCBtn(_ sender: UIButton) {
        
        
        self.performSegue(withIdentifier: "toSecVC", sender: self)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        firstVCTextField.text = textValue ?? "nothing"
    }

    //MARK: - TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        firstVCTextField.endEditing(true)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if firstVCTextField.text != ""{
            return true
        } else {
            firstVCTextField.placeholder = "please enter something"
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        firstVCTextField.endEditing(true)
    }
    
    
    //MARK: - Navigation
    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toSecVC" {
            //access second View Controller
            let destinationVC = segue.destination as! SecondViewController
            //have the second textView value be the firstVC textField text
            destinationVC.secTextValue = firstVCTextField.text ?? "nothing"
        }
    }
    

}

